from django.db import models

# Create your models here.

class Persona(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    edad =  models.IntegerField()
    def __str__(self):
        return "nombre: "+self.nombre+" apellido: " +self.apellido

class Profesor(models.Model):
    rut = models.CharField(max_length=20)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    def __str__(self):
        return "rut: " +self.rut+ " nombre: "+self.nombre+" apellido: " +self.apellido

class Alumno(models.Model):
    rut = models.CharField(max_length=20)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    def __str__(self):
        return "rut: " +self.rut+ " nombre: "+self.nombre+" apellido: " +self.apellido

class Clase(models.Model):
    nombre = models.CharField(max_length=50)
    profesor = models.OneToOneField(Profesor, on_delete=models.CASCADE)
    alumnos = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    def __str__(self):
        return "Clase: " + self.nombre
