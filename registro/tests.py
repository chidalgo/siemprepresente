from django.test import TestCase
from django.test import Client
from .models import Profesor
from .models import Alumno

# Create your tests here.

class ProfesorTestClass(TestCase):
    def test_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code,200)

    def setUp(self):
        profesor = Profesor(rut="1-1",nombre="john",apellido="doe")
        profesor.save()

    def searchProfesorTest(self):
        profesor = Profesor.objects.filter(nombre="john")
        self.assertEqual(profesor[0].nombre,"john")

    def createProfesorTest(self):
        profesor = Profesor(rut="1-1",nombre="john",apellido="doe")
        profesor.save()
        profesor = Profesor(rut="1-1",nombre="john",apellido="doe")
        profesor.save()
        profesor = Profesor(rut="1-1",nombre="john",apellido="doe")
        profesor.save()
        profesor = Profesor(rut="1-1",nombre="john",apellido="doe")
        profesor.save()
        profesor = Profesor(rut="1-1",nombre="john",apellido="doe")
        profesor.save()
        list = Profesor.objects.all()
        self.assertGreaterEqual(len(list),5)
    
class AlumnoTestClass(TestCase):
    def test_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code,200)

    def setUp(self):
        alumno = Alumno(rut="1-1",nombre="john",apellido="doe")
        alumno.save()

    def searchAlumnoTest(self):
        alumno = Alumno.objects.filter(nombre="john")
        self.assertEqual(alumno[0].nombre,"john")

    def createAlumnoTest(self):
        alumno = Alumno(rut="1-1",nombre="john",apellido="doe")
        alumno.save()
        alumno = Alumno(rut="1-1",nombre="john",apellido="doe")
        alumno.save()
        alumno = Alumno(rut="1-1",nombre="john",apellido="doe")
        alumno.save()
        alumno = Alumno(rut="1-1",nombre="john",apellido="doe")
        alumno.save()
        alumno = Alumno(rut="1-1",nombre="john",apellido="doe")
        alumno.save()
        list = Alumno.objects.all()
        self.assertGreaterEqual(len(list),5)
    
