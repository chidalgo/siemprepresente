from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona
from .models import Profesor
from .models import Alumno
from .models import Clase
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import SignUpForm

# Create your views here.

def welcome(request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        return render(request,'index.html', {})
    # En otro caso redireccionamos al login
    return redirect('/login')

class SignUpView(CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'commons/signup.html'

def login(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "login.html", {'form': form})

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('/')

def index(request):
    return render(request,'index.html', {})

def registroPersona(request): 
    return render(request, 'formularioPersona.html', {})

def registroProfesor(request): 
    return render(request, 'formularioProfesor.html', {})

def registroAlumno(request): 
    return render(request, 'formularioAlumno.html', {})

def registroClase(request):
    return render(request, 'formularioClase.html', {})

def listaPersona(request):
    return render(request, 'listadoPersonas.html', {'elementos': Persona.objects.all()})

def listaProfesor(request):
    return render(request, 'listadoProfesores.html', {'elementos': Profesor.objects.all()})

def listaAlumno(request):
    return render(request, 'listadoAlumnos.html', {'elementos': Alumno.objects.all()})

def crearPersona(request):
    nombre = request.POST.get('nombre', '')
    apellido = request.POST.get('apellido', '')
    edad = request.POST.get('edad', 0)
    persona = Persona(nombre=nombre,apellido=apellido,edad=edad)
    persona.save()
    return HttpResponse("nombre: "+nombre+ " apellido: "+apellido)

def buscarPersona(request,id):
    persona = Persona.objects.get(pk=id)
    return HttpResponse("nombre: "+persona.nombre+ " apellido: "+persona.apellido)

def editarPersona(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request, 'editarPersona.html', {'persona' : persona})

def editadoPersona(request,id):
    persona = Persona.objects.get(pk=id)
    nombre = request.POST.get('nombre', '')
    apellido = request.POST.get('apellido', '')
    edad = request.POST.get('edad', 0)
    persona.nombre = nombre
    persona.apellido = apellido
    persona.edad = edad
    persona.save()
    return HttpResponse("nombre: "+persona.nombre+ " apellido: "+persona.apellido)

def eliminarPersona(request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return HttpResponse("Persona eliminada")

def crearProfesor(request):
    rut = request.POST.get('rut','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')

    profesor = Profesor(rut=rut, nombre=nombre, apellido=apellido)
    profesor.save()
    return HttpResponse("El profesor [" + nombre + " " +apellido + "] ha sido creado")

def buscarProfesor(request,id):
    profesor = Profesor.objects.get(pk=id)
    return HttpResponse("rut: "+ profesor.rut +" nombre: "+profesor.nombre+ " apellido: "+profesor.apellido)

def editarProfesor(request,id):
    profesor = Profesor.objects.get(pk=id)
    return render(request, 'editarProfesor.html', {'profesor' : profesor})

def editadoProfesor(request,id):
    profesor = Profesor.objects.get(pk=id)
    rut = request.POST.get('rut','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    profesor.rut = rut
    profesor.nombre = nombre
    profesor.apellido = apellido
    profesor.save()
    return HttpResponse("rut: "+ profesor.rut +" nombre: "+profesor.nombre+ " apellido: "+profesor.apellido)

def eliminarProfesor(request,id):
    profesor = Profesor.objects.get(pk=id)
    profesor.delete()
    return HttpResponse("Profesor eliminado")
    
def crearAlumno(request):
    rut = request.POST.get('rut','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')

    alumno = Alumno(rut=rut, nombre=nombre, apellido=apellido)
    alumno.save()
    return HttpResponse("El alumno [" + nombre + " " +apellido + "] ha sido creado")

def buscarAlumno(request,id):
    alumno = Alumno.objects.get(pk=id)
    return HttpResponse("rut: "+ alumno.rut +" nombre: "+alumno.nombre+ " apellido: "+alumno.apellido)

def editarAlumno(request,id):
    alumno = Alumno.objects.get(pk=id)
    return render(request, 'editarAlumno.html', {'alumno' : alumno})

def editadoAlumno(request,id):
    alumno = Alumno.objects.get(pk=id)
    rut = request.POST.get('rut','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    alumno.rut = rut
    alumno.nombre = nombre
    alumno.apellido = apellido
    alumno.save()
    return HttpResponse("rut: "+ alumno.rut +" nombre: "+alumno.nombre+ " apellido: "+alumno.apellido)

def eliminarAlumno(request,id):
    alumno = Alumno.objects.get(pk=id)
    alumno.delete()
    return HttpResponse("Alumno eliminado")

def crearClase(request, Profesor):
    nombre = request.POST.get('nombre','')
    profesor = Profesor.get()
    alumnos = []

    clase = Clase(nombre=nombre, profesor=profesor, alumnos=alumnos)
    clase.save()
    return HttpResponse("Clase [" + nombre + "] ha sido creada")

def buscarClase(request,id):
    clase = Clase.objects.get(pk=id)
    return HttpResponse("Nombre de la clase: " + clase.nombre)

def editarClase(request,id):
    clase = Clase.objects.gget(pk=id)
    return render(request, "editar.html", {'clase' : clase})

def editadoClase(request,id):
    clase = Clase.objects.get(pk=id)
    nombre = request.POST.get('nombre', '')
    clase.nombre = nombre
    clase.save()
    return HttpResponse("La clase ha sido Modificada")

def eliminarClase(request,id):
    clase = Clase.objects.get(pk=id)
    clase.delete()
    return HttpResponse("Clase eliminada")

def agregarAlumnoAClase(request,id,Alumno):
    clase = Clase.objects.get(pk=id)
    alumnos = list.insert(Alumno.get())
    clase.save()
    return HttpResponse("Alumno agregado a la clase")

def eliminarAlumnoDeClase(request,id,Alumno):
    clase = Clase.objects.get(pk=id)
    alumnos = list.remove(Alumno.get())
    clase.save()
    return HttpResponse("Alumno quitado de la clase")
   
