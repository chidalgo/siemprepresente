"""proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from registro import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('registro.urls')),
    path('registro/buscarPersona/<int:id>', views.buscarPersona , name="buscarPersona"),
    path('registro/editarPersona/<int:id>', views.editarPersona, name="editarPersona"),
    path('registro/editadoPersona/<int:id>', views.editadoPersona, name="editadoPersona"),
    path('registro/eliminarPersona/<int:id>', views.eliminarPersona, name= "eliminarPersona"),

    path('registro/buscarProfesor/<int:id>', views.buscarProfesor , name="buscarProfesor"),
    path('registro/editarProfesor/<int:id>', views.editarProfesor, name="editarProfesor"),
    path('registro/editadoProfesor/<int:id>', views.editadoProfesor, name="editadoProfesor"),
    path('registro/eliminarProfesor/<int:id>', views.eliminarProfesor, name= "eliminarProfesor"),

    path('registro/buscarAlumno/<int:id>', views.buscarAlumno , name="buscarAlumno"),
    path('registro/editarAlumno/<int:id>', views.editarAlumno, name="editarAlumno"),
    path('registro/editadoAlumno/<int:id>', views.editadoAlumno, name="editadoAlumno"),
    path('registro/eliminarAlumno/<int:id>', views.eliminarAlumno, name= "eliminarAlumno"),
]
